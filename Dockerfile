FROM python:3.7
WORKDIR /usr/game

COPY /* ./
COPY /requirements.txt ./

RUN python3 -m pip install -r requirements.txt

CMD [ "python3", "./game.py" ]